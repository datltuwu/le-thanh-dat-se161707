﻿using System.Text;

var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        if (string.IsNullOrEmpty(value1) && string.IsNullOrEmpty(value2)) return "";

        StringBuilder resultString = new StringBuilder("");


        while (value1.Length is not 0 && value2.Length is not 0)
        {
            if (value1.Length is not 0)
            {
                resultString.Append(value1.First());
                value1 = value1.Remove(0, 1);
            }
            if (value1.Length is not 0)
            {
                resultString.Append(value2.First());
                value2 = value2.Remove(0, 1);
            }
        }

        if (string.IsNullOrEmpty(value1))
            return resultString.Append(value2).ToString();
        else if (string.IsNullOrEmpty(value2))
            return resultString.Append(value1).ToString();

        return resultString.ToString();
    }
}

