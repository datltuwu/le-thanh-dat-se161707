﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] numberArray)
    {
        if (numberArray is null) return null;

        List<int> numList = new List<int>(numberArray);

        numList.OrderByDescending(x => x);
        return numList.ToArray();
    }

}