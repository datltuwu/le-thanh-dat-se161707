﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        if (x.Length < 2) return -1;

        int largestElementIndex = 0;

        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > x[largestElementIndex]) largestElementIndex = i;
        }

        x[largestElementIndex] = int.MinValue;


        largestElementIndex = 0;

        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > x[largestElementIndex]) largestElementIndex = i;
        }

        return largestElementIndex;
    }
}